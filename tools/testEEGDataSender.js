var request=require('request');
var index=1;

setInterval(function(){
	eeg={
		sample: {
			"attention":Math.floor(Math.random()*100),
			"meditation":Math.floor(Math.random()*100),
			"delta":Math.floor(Math.random()*1286296),
			"theta":Math.floor(Math.random()*131856),
			"lowAlpha":Math.floor(Math.random()*34032),
			"highAlpha":Math.floor(Math.random()*48669),
			"lowBeta":Math.floor(Math.random()*57788),
			"highBeta":Math.floor(Math.random()*25102),
			"lowGamma":Math.floor(Math.random()*8087),
			"highGamma":Math.floor(Math.random()*7358),
			"poorSignalLevel":Math.floor(Math.random()*255),
			"timeStamp":new Date().getTime()
		}
	};
	request.post("http://imsofa.rocks:8080/LabUtils/ws/app-messages", {
		json: {
			applicationId: "eeg-192.168.0.7",
			content: JSON.stringify(eeg.sample),
		}
	});
}, 1000);
