var math = require('mathjs');
var request = require('request-promise');

var INTERVAL = 5000;
var lastDataDumpTime = -1;

var startTime = 0;
var max = [100.0, 100.0, 1286296, 131856, 34032, 48669.5, 57788, 25102.5, 8087, 7358.5, 100, 100, 100, 100, 100, 100];
var colors = ["black", "blue", "red", "purple", "BlueViolet", "Brown", "CadetBlue", "DarkCyan", "green", "DarkSlateBlue", "DarkSlateGray", "DeepPink", "gray", "indigo", "skyblue", "orange", "yellow"];
var labels = ["attention", "meditation", "delta", "theta", "lowAlpha", "highAlpha", "lowBeta", "highBeta", "lowGamma", "highGamma", "mlel", "mrer", "nlml", "nrmr", "nlel", "nrer"];
var dataConfigs = {};
var datasets = [];
var rawData = {};

for (let i in labels) {
    let name = labels[i];
    dataConfigs[name] = {
        data: [],
        label: name,
        fill: false,
        backgroundColor: colors[i],
        borderColor: colors[i],
        borderWidth: 1
    };
    rawData[name] = [];
    datasets.push(dataConfigs[name]);
}

function pushNewData(timeStamp, datasets) {
    console.log(datasets);
}

function updateData() {
    let promise = request("http://imsofa.rocks:8080/LabUtils/ws/app-messages/eeg-192.168.0.7/client1");

    promise.then(function(data) {
        if (data) {
			data=JSON.parse(data);
            data = JSON.parse(data.content);
            if (startTime == 0) {
                startTime = data.timeStamp;
            }
            for (var i = 0; i <= 9; i++) {
                rawData[labels[i]].push(data[labels[i]]);
            }
            if (lastDataDumpTime == -1) {
                lastDataDumpTime = data.timeStamp;
            }
            return request("http://imsofa.rocks:8080/LabUtils/ws/app-messages/face-id127.1.1.1/client1");
        } else {
            return null;
        }
    }).then(function(data) {
        if (data) {
            data = JSON.parse(JSON.parse(data).content);
            let area = data.largestFaceBoundingBox.width * data.largestFaceBoundingBox.height;
            let mlel = (Math.pow(data.landmarks.MouthLeft[0] - data.landmarks.EyeLeftOuter[0], 2) + Math.pow(data.landmarks.MouthLeft[1] - data.landmarks.EyeLeftOuter[1], 2)) * 100 / area;
            let mrer = (Math.pow(data.landmarks.MouthRight[0] - data.landmarks.EyeRightOuter[0], 2) + Math.pow(data.landmarks.MouthRight[1] - data.landmarks.EyeRightOuter[1], 2)) * 100 / area;
            let nlml = (Math.pow(data.landmarks.NoseLeftAlarOutTip[0] - data.landmarks.MouthLeft[0], 2) + Math.pow(data.landmarks.NoseLeftAlarOutTip[1] - data.landmarks.MouthLeft[1], 2)) * 100 / area;
            let nrmr = (Math.pow(data.landmarks.NoseRightAlarOutTip[0] - data.landmarks.MouthRight[0], 2) + Math.pow(data.landmarks.NoseRightAlarOutTip[1] - data.landmarks.MouthRight[1], 2)) * 100 / area;
            let nlel = (Math.pow(data.landmarks.NoseLeftAlarOutTip[0] - data.landmarks.EyeLeftOuter[0], 2) + Math.pow(data.landmarks.NoseLeftAlarOutTip[1] - data.landmarks.EyeLeftOuter[1], 2)) * 100 / area;
            let nrer = (Math.pow(data.landmarks.NoseRightAlarOutTip[0] - data.landmarks.EyeRightOuter[0], 2) + Math.pow(data.landmarks.NoseRightAlarOutTip[1] - data.landmarks.EyeRightOuter[1], 2)) * 100 / area;
            let faceData = [mlel, mrer, nlml, nrmr, nlel, nrer];
            for (var i = 10; i <= 15; i++) {
                rawData[labels[i]].push(faceData[i - 10]);
            }
            if ((data.timestamp - lastDataDumpTime) >= INTERVAL) {
                //collect data
                let newDataSet = {};
                for (var i = 0; i <= 15; i++) {
                    let med = math.median(rawData[labels[i]]);
                    newDataSet[labels[i]] = med;
                    rawData[labels[i]] = [];
                }
                pushNewData(data.timestamp - startTime, newDataSet);
                lastDataDumpTime = data.timestamp;
            }
        }
        setTimeout(function() {
            updateData();
        }, 500);
    }).catch(function(err) {
		console.log(err);
        setTimeout(function() {
            updateData();
        }, 500);
    });
}

setTimeout(function() {
    updateData();
}, 500);
