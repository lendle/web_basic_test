<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.bundle.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/mathjs/5.2.0/math.js"></script>
    </head>
    <body>
        <div style="width: 500px; height: 500px">
            <canvas id="myChart" width="100%" height="90%"></canvas>
        </div>
        <script>
            var INTERVAL = 5000;
            var lastDataDumpTime = -1;

            var startTime = 0;
            var max = [100.0, 100.0, 1286296, 131856, 34032, 48669.5, 57788, 25102.5, 8087, 7358.5, 100, 100, 100, 100, 100, 100];
            var ctx = document.getElementById("myChart");
            var colors = ["black", "blue", "red", "purple", "BlueViolet", "Brown", "CadetBlue", "DarkCyan", "green", "DarkSlateBlue", "DarkSlateGray", "DeepPink", "gray", "indigo", "skyblue", "orange", "yellow"];
            var labels = ["attention", "meditation", "delta", "theta", "lowAlpha", "highAlpha", "lowBeta", "highBeta", "lowGamma", "highGamma", "mlel", "mrer", "nlml", "nrmr", "nlel", "nrer"];
            var dataConfigs = {};
            var datasets = [];
            var rawData = {};

            for (let i in labels) {
                let name = labels[i];
                dataConfigs[name] = {
                    data: [],
                    label: name,
                    fill: false,
                    backgroundColor: colors[i],
                    borderColor: colors[i],
                    borderWidth: 1
                };
                rawData[name] = [];
                datasets.push(dataConfigs[name]);
            }


            var myLineChart = new Chart(ctx, {
                type: 'line',
                data: {
                    "labels": [],
                    "datasets": datasets,
                },
                options: {
                    responsive: true,
                    scales: {
                        yAxes: [{
                                display: true,
                                ticks: {
                                    min: 0,
                                    max: 100
                                }
                            }]
                    },
                    plugins: {
                        datalabels: {
                            display: function (context) {
                                return !context.dataset._meta[0].hidden;
                            }
                        }
                    }
                }
            });

            function pushNewEEGData(timeStamp, datasets) {
                console.log(datasets);
                //datasets: map of object with key=data label and value=data
                if (myLineChart.data.labels.length >= 50) {
                    myLineChart.data.labels.pop();
                    for (let i in labels) {
                        myLineChart.data.datasets[i].data.pop();
                    }
                }

                for (let i in labels) {
                    if (datasets[labels[i]] > max[i]) {
                        max[i] = datasets[labels[i]];
                    }
                    $("#max" + i).text(max[i]);
                }

                myLineChart.data.labels.push(Math.round(timeStamp / 1000));
                for (let i in labels) {
                    myLineChart.data.datasets[i].data.push(datasets[labels[i]] * 100 / max[i]);
                }
                myLineChart.update();
            }

            function updateData() {
                let promise = $.ajax("http://imsofa.rocks:8080/LabUtils/ws/app-messages/eeg-192.168.0.7/client1");

                promise.then(function (data) {
                    if (data && data.content.length > 5) {
                        data = JSON.parse(data.content);
                        if (startTime == 0) {
                            startTime = data.timeStamp;
                        }
                        for (var i = 0; i <= 9; i++) {
                            rawData[labels[i]].push(data[labels[i]]);
                        }
                        if (lastDataDumpTime == -1) {
                            lastDataDumpTime = data.timeStamp;
                        }
                        return $.ajax("http://imsofa.rocks:8080/LabUtils/ws/app-messages/face-id127.1.1.1/client1");
                    } else {
                        return null;
                    }
                }
                ).then(function (data) {
                    if (data && data.content.length > 5) {
                        data = JSON.parse(data.content);
                        console.log(data);
                        let area = data.largestFaceBoundingBox.width * data.largestFaceBoundingBox.height;
                        let mlel = (Math.pow(data.landmarks.MouthLeft[0] - data.landmarks.EyeLeftOuter[0], 2) + Math.pow(data.landmarks.MouthLeft[1] - data.landmarks.EyeLeftOuter[1], 2)) * 100 / area;
                        let mrer = (Math.pow(data.landmarks.MouthRight[0] - data.landmarks.EyeRightOuter[0], 2) + Math.pow(data.landmarks.MouthRight[1] - data.landmarks.EyeRightOuter[1], 2)) * 100 / area;
                        let nlml = (Math.pow(data.landmarks.NoseLeftAlarOutTip[0] - data.landmarks.MouthLeft[0], 2) + Math.pow(data.landmarks.NoseLeftAlarOutTip[1] - data.landmarks.MouthLeft[1], 2)) * 100 / area;
                        let nrmr = (Math.pow(data.landmarks.NoseRightAlarOutTip[0] - data.landmarks.MouthRight[0], 2) + Math.pow(data.landmarks.NoseRightAlarOutTip[1] - data.landmarks.MouthRight[1], 2)) * 100 / area;
                        let nlel = (Math.pow(data.landmarks.NoseLeftAlarOutTip[0] - data.landmarks.EyeLeftOuter[0], 2) + Math.pow(data.landmarks.NoseLeftAlarOutTip[1] - data.landmarks.EyeLeftOuter[1], 2)) * 100 / area;
                        let nrer = (Math.pow(data.landmarks.NoseRightAlarOutTip[0] - data.landmarks.EyeRightOuter[0], 2) + Math.pow(data.landmarks.NoseRightAlarOutTip[1] - data.landmarks.EyeRightOuter[1], 2)) * 100 / area;
                        let faceData = [mlel, mrer, nlml, nrmr, nlel, nrer];
                        for (var i = 10; i <= 15; i++) {
                            rawData[labels[i]].push(faceData[i-10]);
                        }
                        if ((data.timestamp - lastDataDumpTime) >= INTERVAL) {
                            //collect data
                            let newDataSet = {};
                            for (var i = 0; i<=15; i++) {
                                console.log(i+":"+labels[i]+":"+rawData[labels[i]]);
                                let med = math.median(rawData[labels[i]]);
                                newDataSet[labels[i]] = med;
                                rawData[labels[i]] = [];
                            }
                            pushNewEEGData(data.timestamp - startTime, newDataSet);
                            lastDataDumpTime = data.timestamp;
                        }
                    }
                    setTimeout(function () {
                        updateData();
                    }, 500);
                }).fail(function () {
                    setTimeout(function () {
                        updateData();
                    }, 500);
                }
                );
            }

            function updateFaceData() {
                let promise = $.ajax("http://imsofa.rocks:8080/LabUtils/ws/app-messages/face-id127.1.1.1/client1"/*, {
                 success: function (data) {
                 if (data && data.content.length > 5) {
                 data = JSON.parse(data.content);
                 if (startTime == 0) {
                 startTime = data.timeStamp;
                 }
                 for (let i in labels) {
                 rawData[labels[i]].push(data[labels[i]]);
                 }
                 if (lastDataDumpTime == -1) {
                 lastDataDumpTime = data.timeStamp;
                 } else {
                 if ((data.timeStamp - lastDataDumpTime) >= INTERVAL) {
                 //collect data
                 let newDataSet = {};
                 for (let i in labels) {
                 let med = math.median(rawData[labels[i]]);
                 newDataSet[labels[i]] = med;
                 rawData[labels[i]] = [];
                 }
                 pushNewEEGData(data.timeStamp - startTime, newDataSet);
                 lastDataDumpTime = data.timeStamp;
                 }
                 }
                 }
                 setTimeout(function () {
                 updateFaceData();
                 }, 500);
                 }
                 }*/);

                promise.done(function (data) {
                    console.log(data);
                    setTimeout(function () {
                        updateFaceData();
                    }, 500);
                });
            }

            setTimeout(function () {
                updateData();
            }, 500);
        </script>
        <div>
            <ul>
                <li>max: <span id="max0"></span></li>
                <li>max: <span id="max1"></span></li>
                <li>max: <span id="max2"></span></li>
                <li>max: <span id="max3"></span></li>
                <li>max: <span id="max4"></span></li>
                <li>max: <span id="max5"></span></li>
                <li>max: <span id="max6"></span></li>
                <li>max: <span id="max7"></span></li>
                <li>max: <span id="max8"></span></li>
                <li>max: <span id="max9"></span></li>
            </ul>
        </div>
    </body>
</html>