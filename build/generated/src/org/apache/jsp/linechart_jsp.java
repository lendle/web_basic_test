package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class linechart_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\"\n");
      out.write("    \"http://www.w3.org/TR/html4/loose.dtd\">\n");
      out.write("\n");
      out.write("<html>\n");
      out.write("    <head>\n");
      out.write("        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n");
      out.write("        <title>JSP Page</title>\n");
      out.write("        <script src=\"https://code.jquery.com/jquery-3.3.1.js\"></script>\n");
      out.write("        <script src=\"https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.bundle.js\"></script>\n");
      out.write("        <script src=\"https://cdnjs.cloudflare.com/ajax/libs/mathjs/5.2.0/math.js\"></script>\n");
      out.write("    </head>\n");
      out.write("    <body>\n");
      out.write("        <div style=\"width: 500px; height: 500px\">\n");
      out.write("            <canvas id=\"myChart\" width=\"100%\" height=\"90%\"></canvas>\n");
      out.write("        </div>\n");
      out.write("        <script>\n");
      out.write("            var INTERVAL = 5000;\n");
      out.write("            var lastDataDumpTime = -1;\n");
      out.write("\n");
      out.write("            var startTime = 0;\n");
      out.write("            var max = [100.0, 100.0, 1286296, 131856, 34032, 48669.5, 57788, 25102.5, 8087, 7358.5, 100, 100, 100, 100, 100, 100];\n");
      out.write("            var ctx = document.getElementById(\"myChart\");\n");
      out.write("            var colors = [\"black\", \"blue\", \"red\", \"purple\", \"BlueViolet\", \"Brown\", \"CadetBlue\", \"DarkCyan\", \"green\", \"DarkSlateBlue\", \"DarkSlateGray\", \"DeepPink\", \"gray\", \"indigo\", \"skyblue\", \"orange\", \"yellow\"];\n");
      out.write("            var labels = [\"attention\", \"meditation\", \"delta\", \"theta\", \"lowAlpha\", \"highAlpha\", \"lowBeta\", \"highBeta\", \"lowGamma\", \"highGamma\", \"mlel\", \"mrer\", \"nlml\", \"nrmr\", \"nrel\", \"nrer\"];\n");
      out.write("            var dataConfigs = {};\n");
      out.write("            var datasets = [];\n");
      out.write("            var rawData = {};\n");
      out.write("\n");
      out.write("            for (let i in labels) {\n");
      out.write("                let name = labels[i];\n");
      out.write("                dataConfigs[name] = {\n");
      out.write("                    data: [],\n");
      out.write("                    label: name,\n");
      out.write("                    fill: false,\n");
      out.write("                    backgroundColor: colors[i],\n");
      out.write("                    borderColor: colors[i],\n");
      out.write("                    borderWidth: 1\n");
      out.write("                };\n");
      out.write("                rawData[name] = [];\n");
      out.write("                datasets.push(dataConfigs[name]);\n");
      out.write("            }\n");
      out.write("\n");
      out.write("\n");
      out.write("            var myLineChart = new Chart(ctx, {\n");
      out.write("                type: 'line',\n");
      out.write("                data: {\n");
      out.write("                    \"labels\": [],\n");
      out.write("                    \"datasets\": datasets,\n");
      out.write("                },\n");
      out.write("                options: {\n");
      out.write("                    responsive: true,\n");
      out.write("                    scales: {\n");
      out.write("                        yAxes: [{\n");
      out.write("                                display: true,\n");
      out.write("                                ticks: {\n");
      out.write("                                    min: 0,\n");
      out.write("                                    max: 100\n");
      out.write("                                }\n");
      out.write("                            }]\n");
      out.write("                    },\n");
      out.write("                    plugins: {\n");
      out.write("                        datalabels: {\n");
      out.write("                            display: function (context) {\n");
      out.write("                                return !context.dataset._meta[0].hidden;\n");
      out.write("                            }\n");
      out.write("                        }\n");
      out.write("                    }\n");
      out.write("                }\n");
      out.write("            });\n");
      out.write("\n");
      out.write("            function pushNewEEGData(timeStamp, datasets) {\n");
      out.write("                //datasets: map of object with key=data label and value=data\n");
      out.write("                if (myLineChart.data.labels.length >= 50) {\n");
      out.write("                    myLineChart.data.labels.pop();\n");
      out.write("                    for (let i in labels) {\n");
      out.write("                        myLineChart.data.datasets[i].data.pop();\n");
      out.write("                    }\n");
      out.write("                }\n");
      out.write("\n");
      out.write("                for (let i in labels) {\n");
      out.write("                    if (datasets[labels[i]] > max[i]) {\n");
      out.write("                        max[i] = datasets[labels[i]];\n");
      out.write("                    }\n");
      out.write("                    $(\"#max\" + i).text(max[i]);\n");
      out.write("                }\n");
      out.write("\n");
      out.write("                myLineChart.data.labels.push(Math.round(timeStamp / 1000));\n");
      out.write("                for (let i in labels) {\n");
      out.write("                    myLineChart.data.datasets[i].data.push(datasets[labels[i]] * 100 / max[i]);\n");
      out.write("                }\n");
      out.write("                myLineChart.update();\n");
      out.write("            }\n");
      out.write("\n");
      out.write("            function updateData() {\n");
      out.write("                let promise = $.ajax(\"http://imsofa.rocks:8080/LabUtils/ws/app-messages/eeg-192.168.0.7/client1\");\n");
      out.write("\n");
      out.write("                promise.then(function (data) {\n");
      out.write("                    if (data && data.content.length > 5) {\n");
      out.write("                        data = JSON.parse(data.content);\n");
      out.write("                        if (startTime == 0) {\n");
      out.write("                            startTime = data.timeStamp;\n");
      out.write("                        }\n");
      out.write("                        for (var i = 0; i <= 9; i++) {\n");
      out.write("                            rawData[labels[i]].push(data[labels[i]]);\n");
      out.write("                        }\n");
      out.write("                        if (lastDataDumpTime == -1) {\n");
      out.write("                            lastDataDumpTime = data.timeStamp;\n");
      out.write("                        } else {\n");
      out.write("                            if ((data.timeStamp - lastDataDumpTime) >= INTERVAL) {\n");
      out.write("                                //collect data\n");
      out.write("                                let newDataSet = {};\n");
      out.write("                                for (var i = 0; i <= 9; i++) {\n");
      out.write("                                    let med = math.median(rawData[labels[i]]);\n");
      out.write("                                    newDataSet[labels[i]] = med;\n");
      out.write("                                    rawData[labels[i]] = [];\n");
      out.write("                                }\n");
      out.write("                                pushNewEEGData(data.timeStamp - startTime, newDataSet);\n");
      out.write("                                lastDataDumpTime = data.timeStamp;\n");
      out.write("                            }\n");
      out.write("                        }\n");
      out.write("                        return $.ajax(\"http://imsofa.rocks:8080/LabUtils/ws/app-messages/face-id127.1.1.1/client1\");\n");
      out.write("                    } else {\n");
      out.write("                        return null;\n");
      out.write("                    }\n");
      out.write("                }\n");
      out.write("                ).then(function (data) {\n");
      out.write("                    if (data && data.content.length > 5) {\n");
      out.write("                        data = JSON.parse(data.content);\n");
      out.write("                        console.log(data);\n");
      out.write("                    }\n");
      out.write("                    setTimeout(function () {\n");
      out.write("                        updateData();\n");
      out.write("                    }, 500);\n");
      out.write("                }).fail(\n");
      out.write("                    setTimeout(function () {\n");
      out.write("                        updateData();\n");
      out.write("                    }, 500);    \n");
      out.write("                );\n");
      out.write("            }\n");
      out.write("\n");
      out.write("            function updateFaceData() {\n");
      out.write("                let promise = $.ajax(\"http://imsofa.rocks:8080/LabUtils/ws/app-messages/face-id127.1.1.1/client1\"/*, {\n");
      out.write("                 success: function (data) {\n");
      out.write("                 if (data && data.content.length > 5) {\n");
      out.write("                 data = JSON.parse(data.content);\n");
      out.write("                 if (startTime == 0) {\n");
      out.write("                 startTime = data.timeStamp;\n");
      out.write("                 }\n");
      out.write("                 for (let i in labels) {\n");
      out.write("                 rawData[labels[i]].push(data[labels[i]]);\n");
      out.write("                 }\n");
      out.write("                 if (lastDataDumpTime == -1) {\n");
      out.write("                 lastDataDumpTime = data.timeStamp;\n");
      out.write("                 } else {\n");
      out.write("                 if ((data.timeStamp - lastDataDumpTime) >= INTERVAL) {\n");
      out.write("                 //collect data\n");
      out.write("                 let newDataSet = {};\n");
      out.write("                 for (let i in labels) {\n");
      out.write("                 let med = math.median(rawData[labels[i]]);\n");
      out.write("                 newDataSet[labels[i]] = med;\n");
      out.write("                 rawData[labels[i]] = [];\n");
      out.write("                 }\n");
      out.write("                 pushNewEEGData(data.timeStamp - startTime, newDataSet);\n");
      out.write("                 lastDataDumpTime = data.timeStamp;\n");
      out.write("                 }\n");
      out.write("                 }\n");
      out.write("                 }\n");
      out.write("                 setTimeout(function () {\n");
      out.write("                 updateFaceData();\n");
      out.write("                 }, 500);\n");
      out.write("                 }\n");
      out.write("                 }*/);\n");
      out.write("\n");
      out.write("                promise.done(function (data) {\n");
      out.write("                    console.log(data);\n");
      out.write("                    setTimeout(function () {\n");
      out.write("                        updateFaceData();\n");
      out.write("                    }, 500);\n");
      out.write("                });\n");
      out.write("            }\n");
      out.write("\n");
      out.write("            setTimeout(function () {\n");
      out.write("                updateData();\n");
      out.write("            }, 500);\n");
      out.write("        </script>\n");
      out.write("        <div>\n");
      out.write("            <ul>\n");
      out.write("                <li>max: <span id=\"max0\"></span></li>\n");
      out.write("                <li>max: <span id=\"max1\"></span></li>\n");
      out.write("                <li>max: <span id=\"max2\"></span></li>\n");
      out.write("                <li>max: <span id=\"max3\"></span></li>\n");
      out.write("                <li>max: <span id=\"max4\"></span></li>\n");
      out.write("                <li>max: <span id=\"max5\"></span></li>\n");
      out.write("                <li>max: <span id=\"max6\"></span></li>\n");
      out.write("                <li>max: <span id=\"max7\"></span></li>\n");
      out.write("                <li>max: <span id=\"max8\"></span></li>\n");
      out.write("                <li>max: <span id=\"max9\"></span></li>\n");
      out.write("            </ul>\n");
      out.write("        </div>\n");
      out.write("    </body>\n");
      out.write("</html>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
